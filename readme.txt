=== Cpl Libcal Block ===
Contributors:      Will Skora and the CPL team
Tags:              block, libcal, springshare
Tested up to:      6.4
Stable tag:        1.2.0
Requires PHP:      7.2
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html
Plugin URI:		   https://gitlab.com/cpl/cpl-libcal-block

== Description ==

A block (used in WordPress/Gutenberg) that fetches and displays your library's events from Springshare's Libcal service.

== Installation ==

This plugin requires Advanced Custom Fields Pro 6.0+ plugin installed and activated.

You need to be authenticated with a token every time you interact with the Libcal API; so you either can pass a token everytime
or setup a private proxy server where the token is stored.
This can be managed/fulfilled through different ways, I use https://github.com/BradCoffield/auth-server-for-springshare (this requires a paid Heroku account, $10/month as of this writing)


1. Upload the plugin files to the `/wp-content/plugins/cpl-libcal-block` directory, or install the plugin through the WordPress plugins screen directly.
1. Create the custom fields (an example is available at https://gist.github.com/skorasaurus/f4b295e73768114f9a851b8da4746a98).
1. Edit the value of $my_api_base_url in cpl-libcal-block.php to match your API
1. Modify the custom fields names in cpl-libcal-block.php to match yours
1. Activate the plugin through the 'Plugins' screen in WordPress

== Customization ==
If you are only changing the API URL or the custom field names; you only need to edit the cpl-libcal-block.php file.

If you wish to change the CSS; you will need to run a [couple build scripts](https://developer.wordpress.org/block-editor/reference-guides/packages/packages-scripts/).
as noted below.

Very briefly:
1. clone the repository into the plugins folder of your WordPress installation (do not do this on a live website)
1. run `npm run start` in this directory
1. make your edits to the CSS
1. run `npm run build`.
1. upload the plugin directory (except for the node_modules directory) to your website


== Changelog ==

= 1.2.0 =
* change the layout to a media object pattern
* display the event's featured image from Libcal and if not, display a default image.

= 1.0.8 =
* Add message when no events are displayed.

= 1.0.0 =
* Transition to registering block via block.json; using wp-scripts
* Require version 6+ of ACF Pro

= 0.2.1 =
* clean up; add block.json although it is not currently used.

= 0.1.0 =
* Initial Release

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above. This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation." Arbitrary sections will be shown below the built-in sections outlined above.

Iconography courtesy of the following from the Noun Project:
Anamika Singh (infant) https://thenounproject.com/icon/infant-6428835/
Tiago Pontarolo (child) https://thenounproject.com/icon/child-4933541/
bsd studio (teenager) https://thenounproject.com/icon/teenager-6283054
Gan Khoon Lay (family) https://thenounproject.com/icon/family-1915285/
Lihum Studio (senior) https://thenounproject.com/icon/senior-6052294/
