<?php
/**
 * Plugin Name:       Cpl Libcal Block
 * Description:       Gutenberg block that display CPL's events from Libcal calendar. Requires ACF Pro 6.
 * Requires at least: 6.0
 * Requires PHP:      7.2+
 * Version:           1.2.0
 * Author:            Will Skora and the CPL Team
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       cpl-libcal-block
 *
 * @package           cpl
 */

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
function cpl_cpl_libcal_block_block_init() {
	register_block_type( __DIR__ . '/build' );
}
add_action( 'init', 'cpl_cpl_libcal_block_block_init', 5 );


/**
 * transform_libcal_array_to_string
 *
 * take the input field (indexed array of integers) and returns them as comma separated string
 * e.g. '4242,5232,4333';
 * '&campus=' will return all results;
 * used for the libcal_campuses and libcal_categories fields
 *
 *
 */
function transform_libcal_array_to_string( $teh_input ) {
	// $acf_libcal_campuses_array     = $teh_input;
	$acf_libcal_array_as_string = '';
	foreach ( $teh_input as $xx ) {
		$acf_libcal_array_as_string .= $xx . ',';
	}
	return $acf_libcal_array_as_string;
}

/**
 * remove_main_or_LSW_prefix
 *
 * removes some prefixes that we use at the start of some spaces' names
 * at Main Library Branch
 * .e.g. 'Main 03 - ' and 'LSW 02'
 *
 */

function remove_main_or_LSW_prefix( $teh_input_string ) {
	$result = preg_replace(
		'/(Main|LSW) \d{2} - /',
		'',
		$teh_input_string
	);
	return $result;
}


// per CPL style guide, minutes are only displayed in times when are not on the hour
//
// e.g. 4:00 pm should not be displayed; instead written as 4 pm
// 4:30 pm should be written as 4:30
// converts the date into our preferred format
// this creates a new date object, just isolates minute value, and
// if its is not 0, return a start time that displays the minute value
//
function format_start_time( $input_date ) {

	$minutes_field = date_format( $input_date, 'i' );

	if ( $minutes_field === '00' ) {
		$formatted_date = date_format( $input_date, 'l, F d \| g a' );
	} else {
		$formatted_date = date_format( $input_date, 'l, F d \| g\:i a' );
	}
	return $formatted_date;
}

/*
display_events_featured_image
display a fallback image based on the event's audience

@param {array} $audience_array
typically consists of multiple audiences; each audience will consist of consists of 2 key=value pairs;

e.g. Array
( [0] => stdClass Object ( [id] => 194 [name] => Adults Ages 19+ )
[1] => stdClass Object ( [id] => 1741 [name] => Seniors )
[2] => stdClass Object ( [id] => 1742 [name] => All Ages )
[3] => stdClass Object ( [id] => 4989 [name] => Children Ages 6-11 ) )\

each image only represents 1 audience
per m.young; prioritize the youngest to oldest

array_column pulls out all of the values for the key "id" in the array $audience_array, and returns them as an array

array_search will first look to see if the audience ID (e.g. 1742) exists somewhere in the array
the !== FALSE is there; so that if there is only 1 audience in the audience_array; array_search will still find the audience ID

*/

function display_events_featured_image( $audience_array ) {
	if ( array_search( 1742, array_column( $audience_array, 'id' ) ) !== false ) {
		echo '<img src="' . esc_url( plugins_url( 'img/CPL_TheCenter_icon_2935_rev.png', __FILE__ ) ) . '" alt="Event For all ages">';
	} elseif ( array_search( 195, array_column( $audience_array, 'id' ) ) !== false ) {
			echo '<img src="' . esc_url( plugins_url( 'img/CPL_TheCenter_icon_2935_rev.png', __FILE__ ) ) . '" alt="Event For young children under 6 years old">';
	} elseif ( array_search( 4849, array_column( $audience_array, 'id' ) ) !== false ) {
		echo '<img src="' . esc_url( plugins_url( 'img/CPL_TheCenter_icon_2935_rev.png', __FILE__ ) ) . '" alt="Event For kids">';
	} elseif ( array_search( 196, array_column( $audience_array, 'id' ) ) !== false ) {
		echo '<img src="' . esc_url( plugins_url( 'img/CPL_TheCenter_icon_2935_rev.png', __FILE__ ) ) . '" alt="Event For teens">';
	} elseif ( array_search( 194, array_column( $audience_array, 'id' ) ) !== false ) {
		echo '<img src="' . esc_url( plugins_url( 'img/CPL_TheCenter_icon_2935_rev.png', __FILE__ ) ) . '" alt="Event For adults">';
	} elseif ( array_search( 1741, array_column( $audience_array, 'id' ) ) !== false ) {
			echo '<img src="' . esc_url( plugins_url( 'img/CPL_TheCenter_icon_2935_rev.png', __FILE__ ) ) . '" alt="Event For seniors">';
	} else {
		echo '<img src="' . esc_url( plugins_url( 'img/CPL_TheCenter_icon_2935_rev.png', __FILE__ ) ) . '" alt="">';
	}
}


// 	audience->
//   array( "1742" => "all ages",
// 	"195" => "Children 0-5",
//         "4849" => "Kids 6011",
// 		"196" => "Teens",
// 		"194" => "adults",
// 		"1741" => "seniors"



function block_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {
	$days_in_advance_to_show_var = sanitize_text_field( get_field( 'days_in_advance_to_show' ) );

	$my_api_base_url = 'https://cpl-halva.herokuapp.com/springshare/libcal/passthrough?what=/events?cal_id=8758' . '&tag=' . transform_libcal_array_to_string( get_field( 'libcal_internal_tags' ) ) . '&days=' . $days_in_advance_to_show_var . '&campus=' . transform_libcal_array_to_string( get_field( 'libcal_campuses' ) ) . '&category=' . transform_libcal_array_to_string( get_field( 'libcal_event_categories' ) );

	$initial_response = wp_remote_get( $my_api_base_url );

	if ( is_wp_error( $initial_response ) ) {
		return false; // can I return something else more verbose to the user?
	}

	$body_response = wp_remote_retrieve_body( $initial_response );

	// decodes it into an object
	$response_object = json_decode( $body_response, false );

	// $teh_error = json_last_error();

	// var_dump_pre( $response_object) ;
	// https://pippinsplugins.com/using-wp_remote_get-to-parse-json-from-remote-apis/
	// return 'What a boffo block!';

	// detect if the response is empty; if so, return message
	if ( empty( $response_object->events ) ) {
		?> <p> No events are currently scheduled at this location. </p>
		<?php
	} else {
		?>
			<ul class="cpl-flex--mini">

		<?php

		// iterate through each event in the object
		foreach ( $response_object->events as $event ) {

			// reads the time formatting from libcal, e.g. 2021-05-25T15:15:00-04:00
			$teh_date = \DateTime::createFromFormat( 'Y-m-d\TH:i:sP', $event->start );

			format_start_time( $teh_date );

			?>
			  <div class="media-object-container">
				<div class="media-object__media">
			<?php

			if ( ! empty( $event->featured_image ) ) {
				echo '<img src="' . esc_url( $event->featured_image ) . '" alt="">';
			} else {
				display_events_featured_image( $event->audience );
			}
			?>
				</div>
				<div class=media-object__object>
				<li class="cpl-flex--mini__item">
					<h3 class="cpl-flex--mini__item-name">
					<?php echo $event->title; ?>
					</h3>
				<span class="cpl-flex--mini__item-date">
				<?php

				/**
				 *
				 *
				 * there are 4 scenarios where we want to display different information:
				* - an online event hosted/associated with a particular branch
				* - an online event not associated with any branch
				* - an event physically hosted at any branch except main
				* - an event physically hosted at main
				* - an event physically hosted elsewhere
				* working with the assumption We DO NOT offer hybrid events that offer both online and physical attendence
				*
				*/
				//
				if ( isset( $event->campus->name ) ) {
					if ( ! empty( $event->location->name ) ) {
						if ( $event->campus->id == '4604' ) {
							echo 'Main Library (Downtown)' . ', ' . remove_main_or_LSW_prefix( $event->location->name );
						} else {
							echo $event->campus->name;
						}
					} else {
						echo 'Online Event presented by ' . $event->campus->name;
					}
				} elseif ( $event->location->id == '15402' ) {
					echo 'Off-Site Location (Read Description)';

				} else {
					echo 'Online Event';
				}

				?>
				 </span></br>
			 <?php
				echo format_start_time( $teh_date );
				?>
				<?php

				// if registration is enabled as true; include a registration button
				if ( $event->registration == 'true' ) {

					?>
					<div class="wp-block-buttons">
	<div class="wp-block-button is-style-cpl-button--action"><a class="wp-block-button__link" href="<?php echo $event->url->public; ?>">Register <span class="screen-reader-text"><?php echo ' for ' . $event->title; ?></span></a></div>
	</div>
					<?php
				} // end of if for registration=true

				echo $event->description;

				?>
				</li>
			</div>
			<div class="footer"></div>
</div>
				<?php
		}
	}
	?>
			</ul>

		<?php
}
?>
